import React from 'react';
import './App.css';
import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com'
});

class App extends React.Component {
  state = {
    hello: null
  };

  componentDidMount() {
     axiosInstance.get('/posts')
       .then(res => console.log(res.data))
       .catch(err => console.error(err.message));
  }

  render() {
    return (
      <div>
        {this.state.hello ? <div>{this.state.hello}</div> : null}
      </div>
    )
  }
}

export default App;
